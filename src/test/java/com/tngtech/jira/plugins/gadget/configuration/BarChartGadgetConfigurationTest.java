package com.tngtech.jira.plugins.gadget.configuration;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.jira.issue.fields.CustomField;
import com.tngtech.jira.plugins.gadget.rest.FieldList;
import com.tngtech.jira.plugins.utils.fields.CommonField;
import com.tngtech.jira.plugins.utils.fields.CustomFieldUtils;

public class BarChartGadgetConfigurationTest {

	@Test
	public void shouldReturnOnlyCommonFields() {
		CustomFieldUtils customFieldUtils = mock(CustomFieldUtils.class);
		when(customFieldUtils.getCustomFields()).thenReturn(null);
		BarChartGadgetConfiguration configuration = new BarChartGadgetConfiguration(customFieldUtils);
		Response response = configuration.lookupCustomAndCommonFieldNames();
		List<Map<String, String>> customFieldInfos = ((FieldList) response.getEntity()).fieldNames;

		CommonField[] commonFields = CommonField.getSortedValues();

		assertThat(customFieldInfos, notNullValue());
		assertThat(customFieldInfos.size(), is(commonFields.length));
		for (int i = 0; i < commonFields.length; i++) {
			customFieldListContains(customFieldInfos.get(i), Long.valueOf(commonFields[i].getId()), commonFields[i]
					.getLabel(null));
		}
	}

	@Test
	public void shouldReturnCustomFieldNames() {
		CustomFieldUtils customFieldUtils = mockCustomFieldUtils();
		BarChartGadgetConfiguration configuration = new BarChartGadgetConfiguration(customFieldUtils);
		Response response = configuration.lookupCustomAndCommonFieldNames();

		List<Map<String, String>> customFieldInfos = ((FieldList) response.getEntity()).fieldNames;
		int countCommonFields = CommonField.values().length;

		assertThat(customFieldInfos, notNullValue());
		assertThat(customFieldInfos.size(), is(countCommonFields + 2));
		customFieldListContains(customFieldInfos, 1L, "First Field");
		customFieldListContains(customFieldInfos, 2L, "Second Field");
	}

	@Test
	public void shouldReturnOnlyCustomFieldNames() {
		CustomFieldUtils customFieldUtils = mockCustomFieldUtils();
		BarChartGadgetConfiguration configuration = new BarChartGadgetConfiguration(customFieldUtils);
		Response response = configuration.lookupCustomFieldNames();

		List<Map<String, String>> customFieldInfos = ((FieldList) response.getEntity()).fieldNames;

		assertThat(customFieldInfos, notNullValue());
		assertThat(customFieldInfos.size(), is(2));
		customFieldListContains(customFieldInfos.get(0), 1L, "First Field");
		customFieldListContains(customFieldInfos.get(1), 2L, "Second Field");
	}

	private CustomFieldUtils mockCustomFieldUtils() {
		CustomFieldUtils customFieldUtils = mock(CustomFieldUtils.class);
		List<CustomField> customFields = new ArrayList<CustomField>();
		customFields.add(mockCustomField(1L, "First Field"));
		customFields.add(mockCustomField(2L, "Second Field"));
		when(customFieldUtils.getCustomFields()).thenReturn(customFields);
		return customFieldUtils;
	}

	private CustomField mockCustomField(Long id, String name) {
		CustomField customField = mock(CustomField.class);
		when(customField.getIdAsLong()).thenReturn(id);
		when(customField.getName()).thenReturn(name);
		return customField;
	}

	private void customFieldListContains(Map<String, String> customFieldInfos, Long id, String value) {
		assertThat(customFieldInfos.containsKey(FieldList.LABEL), is(true));
		assertThat(customFieldInfos.get(FieldList.LABEL), is(value.toString()));
		assertThat(customFieldInfos.containsKey(FieldList.VALUE), is(true));
		assertThat(customFieldInfos.get(FieldList.VALUE), is(id.toString()));
	}

	// TODO refactor test
	private void customFieldListContains(List<Map<String, String>> customFieldInfosList, Long id, String value) {
		for (Map<String, String> customFieldInfos : customFieldInfosList) {
			if (customFieldInfos.get(FieldList.VALUE).equals(id.toString())) {
				customFieldListContains(customFieldInfos, id, value);
				return;
			}
		}
		assertThat(false, is(true));
	}

}
