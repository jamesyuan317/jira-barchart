package com.tngtech.jira.plugins.gadget.chart;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.jfree.data.category.DefaultCategoryDataset;
import org.junit.Test;

import com.atlassian.jira.charts.jfreechart.ChartHelper;
import com.atlassian.jira.web.bean.I18nBean;
import com.tngtech.jira.plugins.utils.JiraUtils;

public class BarChartGeneratorTest {

	@Test
	public void shouldGenerateAChartWithoutExceptions() {
		boolean thrown = false;
		try {
			DefaultCategoryDataset dataset = new DefaultCategoryDataset();
			dataset.addValue(1.0, "row", "col");
			JiraUtils jiraUtils = mock(JiraUtils.class);
			I18nBean bean = mock(I18nBean.class);
			when(jiraUtils.geti18nBean()).thenReturn(bean);

			BarChartGenerator generator = new BarChartGenerator(dataset, jiraUtils);
			ChartHelper helper = generator.generateChart();
			helper.getLocation();
		} catch (Throwable t) {
			thrown = true;
		}
		assertThat(thrown, is(false));
	}
}
