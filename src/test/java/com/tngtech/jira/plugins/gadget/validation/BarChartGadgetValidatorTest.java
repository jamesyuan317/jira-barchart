package com.tngtech.jira.plugins.gadget.validation;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.tngtech.jira.plugins.gadget.rest.Message;
import com.tngtech.jira.plugins.utils.JiraUtils;

public class BarChartGadgetValidatorTest {

	@Test
	public void shouldSupportCustomFieldTypes() {
		JiraUtils jiraUtils = mock(JiraUtils.class);
		CustomField customField1 = mockCustomField("com.atlassian.jira.plugin.system.customfieldtypes:select");
		CustomField customField2 = mockCustomField("com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes");
		when(jiraUtils.getCustomFieldById(42L)).thenReturn(customField1);
		when(jiraUtils.getCustomFieldById(1337L)).thenReturn(customField2);
		BarChartGadgetValidator validator = new BarChartGadgetValidator(jiraUtils);

		Message message = (Message) validator.validateInput("42", "1337").getEntity();
		List<String> messages = message.messages;

		assertThat(messages, notNullValue());
		assertThat(messages.size(), is(0));
	}

	@Test
	public void shouldNotSupportCustomFieldTypes() {
		JiraUtils jiraUtils = mock(JiraUtils.class);
		CustomField customField1 = mockCustomField("blabla", "1stname");
		CustomField customField2 = mockCustomField("blupblup", "2ndname");
		when(jiraUtils.getCustomFieldById(42L)).thenReturn(customField1);
		when(jiraUtils.getCustomFieldById(1337L)).thenReturn(customField2);
		BarChartGadgetValidator validator = new BarChartGadgetValidator(jiraUtils);

		Message message = (Message) validator.validateInput("42", "1337").getEntity();
		List<String> messages = message.messages;

		assertThat(messages, notNullValue());
		assertThat(messages.size(), is(2));
		assertThat(messages.get(0).contains("1stname"), is(true));
		assertThat(messages.get(1).contains("2ndname"), is(true));
	}

	@Test
	public void shouldNotReturnEqualMessages() {
		JiraUtils jiraUtils = mock(JiraUtils.class);
		CustomField customField = mockCustomField("blabla", "1stname");
		when(jiraUtils.getCustomFieldById(42L)).thenReturn(customField);
		when(jiraUtils.getCustomFieldById(42L)).thenReturn(customField);
		BarChartGadgetValidator validator = new BarChartGadgetValidator(jiraUtils);

		Message message = (Message) validator.validateInput("42", "42").getEntity();
		List<String> messages = message.messages;

		assertThat(messages, notNullValue());
		assertThat(messages.size(), is(1));
		assertThat(messages.get(0).contains("1stname"), is(true));
	}

	@SuppressWarnings("rawtypes")
	private CustomField mockCustomField(String typeKey) {
		CustomField customField = mock(CustomField.class);
		CustomFieldType type = mock(CustomFieldType.class);
		when(type.getKey()).thenReturn(typeKey);
		when(customField.getCustomFieldType()).thenReturn(type);
		return customField;
	}

	private CustomField mockCustomField(String typeKey, String name) {
		CustomField customField = mockCustomField(typeKey);
		when(customField.getName()).thenReturn(name);
		return customField;
	}

}
