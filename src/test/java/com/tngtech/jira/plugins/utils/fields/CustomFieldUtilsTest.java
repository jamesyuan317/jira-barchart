package com.tngtech.jira.plugins.utils.fields;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.atlassian.jira.issue.fields.CustomField;
import com.tngtech.jira.plugins.utils.JiraUtils;

public class CustomFieldUtilsTest {

	@Test
	public void shouldReturnEmptyList() {
		JiraUtils jiraUtils = mock(JiraUtils.class);
		CustomFieldUtils cfUtils = new CustomFieldUtils(jiraUtils);

		List<CustomField> foundFields = cfUtils.getCustomFields();

		assertThat(foundFields, notNullValue());
		assertThat(foundFields.size(), is(0));
	}

	@Test
	public void shouldReturnRightList() {
		JiraUtils jiraUtils = mock(JiraUtils.class);
		List<CustomField> customFields = new ArrayList<CustomField>();
		when(jiraUtils.getAllCustomFields()).thenReturn(customFields);
		CustomFieldUtils cfUtils = new CustomFieldUtils(jiraUtils);

		List<CustomField> foundFields = cfUtils.getCustomFields();

		assertThat(foundFields, is(customFields));
	}

}
