package it.com.tngtech.jira.plugins.gadget;

import org.junit.Before;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.webtests.util.LocalTestEnvironmentData;

public class BarChartGadgetVisualisationTest extends FuncTestCase {

	public BarChartGadgetVisualisationTest() {
		super();
		setEnvironmentData(new LocalTestEnvironmentData("target/test-classes/xml"));
	}

	@Before
	public void setUpTest() {
		administration.restoreData("chart-test-data.xml");
	}

	public void testNothing() {
		// Usually test html output here
	}

}
