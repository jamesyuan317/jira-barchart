package com.tngtech.jira.plugins.utils.fields;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.issue.fields.CustomField;
import com.tngtech.jira.plugins.utils.JiraUtils;

public class CustomFieldUtils {

	private JiraUtils jiraUtils;

	public CustomFieldUtils(JiraUtils jiraUtils) {
		this.jiraUtils = jiraUtils;
	}

	public List<CustomField> getCustomFields() {
		List<CustomField> customFields = jiraUtils.getAllCustomFields();
		if (customFields == null) {
			return new ArrayList<CustomField>(0);
		}
		return customFields;
	}

}
