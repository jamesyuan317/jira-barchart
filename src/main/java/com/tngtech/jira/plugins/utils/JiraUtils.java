package com.tngtech.jira.plugins.utils;

import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;

public class JiraUtils {

	private final JiraAuthenticationContext authenticationContext;
	private final SearchService searchService;
	private final SearchRequestService searchRequestService;

	public JiraUtils(final SearchService searchService, final JiraAuthenticationContext authenticationContext,
			final SearchRequestService searchRequestService) {
		this.authenticationContext = authenticationContext;
		this.searchService = searchService;
		this.searchRequestService = searchRequestService;
	}

	public User getUser() {
		return authenticationContext.getLoggedInUser();
	}

	public SearchRequest getSearchRequestForFilter(Long filterId) {
		JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(getUser());
		return searchRequestService.getFilter(jiraServiceContext, filterId);
	}

	@SuppressWarnings("rawtypes")
	public SearchResults getSearchResults(Query query, PagerFilter pagerFilter) throws SearchException {
		return searchService.search(getUser(), query, pagerFilter);
	}

	public Project getProjectById(Long projectId) {
		ProjectManager projectManager = ComponentManager.getInstance().getProjectManager();
		return projectManager.getProjectObj(projectId);
	}

	public CustomField getCustomFieldById(Long fieldId) {
		CustomFieldManager customFieldManager = ComponentManager.getInstance().getCustomFieldManager();
		return customFieldManager.getCustomFieldObject(fieldId);
	}

	public CustomField getCustomFieldByName(String fieldName) {
		CustomFieldManager customFieldManager = ComponentManager.getInstance().getCustomFieldManager();
		return customFieldManager.getCustomFieldObjectByName(fieldName);
	}

	public List<Option> getAllCustomFieldOptions() {
		OptionsManager manager = ComponentManager.getComponent(OptionsManager.class);
		return manager.getAllOptions();
	}

	public List<CustomField> getAllCustomFields() {
		CustomFieldManager customFieldManager = ComponentManager.getInstance().getCustomFieldManager();
		return customFieldManager.getCustomFieldObjects();
	}

	public I18nBean geti18nBean() {
		return new I18nBean(getUser());
	}

	public String getTranslatedText(String key) {
		return authenticationContext.getI18nHelper().getText(key);
	}

}
