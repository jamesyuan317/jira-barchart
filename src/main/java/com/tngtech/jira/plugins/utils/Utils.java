package com.tngtech.jira.plugins.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Utils {

	private Utils() {
	}

	public static String mapToString(Map<String, String> map, char seperator) {
		StringBuilder builder = new StringBuilder();
		boolean first = true;

		for (Map.Entry<String, String> entry : map.entrySet()) {
			if (first) {
				first = false;
			} else {
				builder.append(seperator);
			}
			builder.append(entry.getKey());
			builder.append(seperator);
			builder.append(entry.getValue());
		}

		return builder.toString();
	}

	public static Map<String, String> stringToMap(String representation, char seperator) {
		Map<String, String> map = new HashMap<String, String>();
		String[] parts = representation.split(String.valueOf(seperator));
		if (parts.length % 2 == 1) {
			throw new IllegalArgumentException("Representation must contain n*(key+value)");
		}
		for (int i = 0; i < parts.length; i += 2) {
			map.put(parts[i], parts[i + 1]);
		}
		return map;
	}

	public static List<String> createOneElementList(String value) {
		List<String> returnList = new ArrayList<String>();
		returnList.add(value);
		return returnList;
	}

	public static Map<Long, String> sortMapByValue(Map<Long, String> unsortedMap) {
		Map<Long, String> sortedMap = new LinkedHashMap<Long, String>();

		for (int i = 0; i < unsortedMap.size(); i++) {
			Long currentKey = null;
			String currentValue = null;

			for (Map.Entry<Long, String> entry : unsortedMap.entrySet()) {
				if ((currentKey == null || currentValue.compareTo(entry.getValue()) > 0)
						&& !sortedMap.containsKey(entry.getKey())) {
					currentKey = entry.getKey();
					currentValue = entry.getValue();
				}
			}
			sortedMap.put(currentKey, currentValue);
		}

		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	public static List<String> toStringList(Collection collection) {
		List<String> stringList = new ArrayList<String>(collection.size());
		Iterator iterator = collection.iterator();
		while (iterator.hasNext()) {
			stringList.add(iterator.next().toString());
		}
		return stringList;
	}

}
