package com.tngtech.jira.plugins.gadget.validation;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.tngtech.jira.plugins.gadget.rest.Message;
import com.tngtech.jira.plugins.utils.JiraUtils;
import com.tngtech.jira.plugins.utils.fields.CustomFieldWrapper;

@Path("/BarChartValidation")
public class BarChartGadgetValidator {

	private static final Logger LOG = Logger.getLogger(BarChartGadgetValidator.class);

	private final JiraUtils jiraUtils;

	private List<String> validCustomFieldKeys;

	public BarChartGadgetValidator(final SearchService searchService,
			final JiraAuthenticationContext authenticationContext, final SearchRequestService searchRequestService) {
		this(new JiraUtils(searchService, authenticationContext, searchRequestService));
	}

	// For Tests
	public BarChartGadgetValidator(JiraUtils jiraUtils) {
		this.jiraUtils = jiraUtils;
		initializeValidCustomFields();
	}

	private void initializeValidCustomFields() {
		validCustomFieldKeys = new ArrayList<String>();
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:select");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:datetime");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:textfield");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:userpicker");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:datepicker");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:version");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:url");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:textarea");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:grouppicker");
		validCustomFieldKeys.add("com.atlassian.jirafisheyeplugin:hiddenjobswitch");
		validCustomFieldKeys.add("com.atlassian.jirafisheyeplugin:jobcheckbox");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:multiselect");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:project");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:labels");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:multiversion");
		validCustomFieldKeys.add("com.atlassian.jira.plugin.system.customfieldtypes:float");
	}

	@GET
	@Path("/ValidateInput")
	public Response validateInput(@QueryParam("axisField") String xAxisFieldIdString,
			@QueryParam("groupField") String groupFieldIdString) {
		try {
			Message message = createMessageObject(xAxisFieldIdString, groupFieldIdString);
			CacheControl cacheControl = new CacheControl();
			cacheControl.setNoCache(true);
			return Response.ok(message).cacheControl(cacheControl).build();
		} catch (RuntimeException e) {
			LOG.error("Caught Exception while validating user input!", e);
			return Response.serverError().build();
		}
	}

	private Message createMessageObject(String xAxisFieldIdString, String groupFieldIdString) {
		List<String> messages = new ArrayList<String>();
		String messageString = validateField(xAxisFieldIdString);
		if (!messageString.isEmpty()) {
			messages.add(messageString);
		}
		messageString = validateField(groupFieldIdString);
		if (!messageString.isEmpty() && !messages.contains(messageString)) {
			messages.add(messageString);
		}
		Message message = new Message(messages);
		return message;
	}

	private String validateField(String fieldIdString) {
		if (fieldIdString == null || fieldIdString.isEmpty()) {
			return "";
		}
		Long fieldId = Long.valueOf(fieldIdString);
		CustomField customField = new CustomFieldWrapper(jiraUtils, fieldId).getCustomField();
		if (customField == null) {
			return "";
		}
		String customFieldType = customField.getCustomFieldType().getKey();
		if (isValidType(customFieldType)) {
			return "";
		} else {
			String name = customField.getName();
			return "Custom Field " + name + " " + jiraUtils.getTranslatedText("gadget.barchart.config.validation.customfield");
		}
	}

	private boolean isValidType(String customFieldType) {
		return validCustomFieldKeys.contains(customFieldType);
	}

}
