package com.tngtech.jira.plugins.gadget.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message {

	@XmlElement
	public List<String> messages;

	public Message(List<String> messages) {
		this.messages = messages;
	}

}
